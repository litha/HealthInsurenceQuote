package com.emids.healthinsurance.controllers;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.service.InsurencePremiumService;

@RunWith(MockitoJUnitRunner.class)
public class HealthInsuranceControllerTest {

	private MockMvc mockMvc;
	private Customer customer;
	
	@Mock
    private InsurencePremiumService insurencePremiumService;    
    @InjectMocks
    private HealthInsuranceController healthInsuranceController;  
    @Before
    public void setUp() {
    	Customer customer=new Customer();
        MockitoAnnotations.initMocks(this);//initialize controller and mocks
        mockMvc=MockMvcBuilders.standaloneSetup(healthInsuranceController).build();
    } 
    
    @Test
    public void listCustomerTest() throws Exception{
	List<Customer> customers=new ArrayList<Customer>();
	customers.add(new Customer());	
    //Specific Mockito interactions,tell stub to return list of customers.
    when(insurencePremiumService.listCustomer()).thenReturn((List)customers);  	
    MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/insurance");
    this.mockMvc.perform(builder)
           .andExpect(MockMvcResultMatchers.status().isOk()).
           andExpect(MockMvcResultMatchers.view().name("quote"));         	
    }    
}
