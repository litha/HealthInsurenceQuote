<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Health Insurance</title>
</head>
<body>
	<b>Health Insurance Premium</b>
	<c:if test="${!empty customerDetailsList}">
		<table>

			<c:forEach items="${customerDetailsList}" var="customer">
				<tr>
					<td>Name :</td>
					<td>${customer.name}</td>
				</tr>
				<tr>
					<td>Gender :</td>
					<td>${customer.gender}</td>
				</tr>
				<tr>
					<td>Age :</td>
					<td>${customer.age}</td>
				</tr>
			</c:forEach>
		</table>
	Health Insurance Quote is : <c:out value="${getPremium}" />
	</c:if>
</body>
</html>