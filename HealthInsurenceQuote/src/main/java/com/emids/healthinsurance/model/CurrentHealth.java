package com.emids.healthinsurance.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class CurrentHealth {
	
@Id
@GeneratedValue
private int id;	
private int hypertension;
private int bloodPressure;
private int bloodSugar;
private int overweight;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getHypertension() {
	return hypertension;
}
public void setHypertension(int hypertension) {
	this.hypertension = hypertension;
}
public int getBloodPressure() {
	return bloodPressure;
}
public void setBloodPressure(int bloodPressure) {
	this.bloodPressure = bloodPressure;
}
public int getBloodSugar() {
	return bloodSugar;
}
public void setBloodSugar(int bloodSugar) {
	this.bloodSugar = bloodSugar;
}
public int getOverweight() {
	return overweight;
}
public void setOverweight(int overweight) {
	this.overweight = overweight;
}

}
