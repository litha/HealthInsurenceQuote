package com.emids.healthinsurance.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Habits {
	@Id
	@GeneratedValue
	int id;
	int smoking;
	int alcohol;
	int dailyExercise;
	int drugs;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSmoking() {
		return smoking;
	}
	public void setSmoking(int smoking) {
		this.smoking = smoking;
	}
	public int getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(int alcohol) {
		this.alcohol = alcohol;
	}
	public int getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(int dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public int getDrugs() {
		return drugs;
	}
	public void setDrugs(int drugs) {
		this.drugs = drugs;
	}


}