package com.emids.healthinsurance.service;

import java.util.List;

import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.model.Habits;
public interface InsurencePremiumService {    
public double getinsurencePremium();
public Customer getCustomerById(int id);
public Habits getHabitsById(int cus_id);
public CurrentHealth getCurrentHealthById(int cus_id);
public List<Customer> listCustomer();
}