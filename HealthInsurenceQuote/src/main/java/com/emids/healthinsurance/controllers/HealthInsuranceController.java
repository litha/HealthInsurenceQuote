package com.emids.healthinsurance.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.service.InsurencePremiumService;


@Controller
public class HealthInsuranceController {
	private static final Logger logger = LoggerFactory.getLogger(HealthInsuranceController.class);	
	@Autowired(required = true)
	private InsurencePremiumService insurencePremiumService;		
	public void setInsurencePremiumService(InsurencePremiumService insurencePremiumService) {
		this.insurencePremiumService = insurencePremiumService;
	}		
	@RequestMapping(value = "/insurance", method = RequestMethod.GET)
	public String listCustomers(Model model) throws Exception {		
		logger.info("Inside controller method");
		model.addAttribute("customer", new Customer());
		model.addAttribute("customerDetailsList", this.insurencePremiumService.listCustomer());		
		model.addAttribute("getPremium", this.insurencePremiumService.getinsurencePremium());
		return "quote";
	}
}
