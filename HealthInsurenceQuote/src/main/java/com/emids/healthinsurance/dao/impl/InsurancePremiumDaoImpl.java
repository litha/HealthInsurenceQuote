package com.emids.healthinsurance.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurence.dao.InsurancePremiumDao;

@Repository("insurancePremiumDao")
public class InsurancePremiumDaoImpl implements InsurancePremiumDao {
	private static final Logger logger = LoggerFactory.getLogger(InsurancePremiumDaoImpl.class);
	private SessionFactory sessionFactory;
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	protected Session currentSession() {
		return sessionFactory.getCurrentSession();
	}
	int amount;
	public int getAmount(int amount) {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;

	}
	public Customer getCustomerById(int cus_id) {
		Customer cst = (Customer) currentSession().load(Customer.class, new Integer(cus_id));
		return cst;
	}
	public Habits getHabitsById(int cus_id) {
		Habits cst = (Habits) currentSession().load(Habits.class, new Integer(cus_id));
		return cst;
	}

	public CurrentHealth getCurrentHealthById(int cus_id) {
		CurrentHealth cst = (CurrentHealth) currentSession().load(CurrentHealth.class, new Integer(cus_id));
		return cst;
	}
	public List<Customer> listCustomer() {
		@SuppressWarnings("unchecked")
		List<Customer> customerList = currentSession().createQuery("from Customer").list();
		for (Customer c : customerList) {
			logger.info("Customer List::" + c);
		}
		return customerList;
	}
}
