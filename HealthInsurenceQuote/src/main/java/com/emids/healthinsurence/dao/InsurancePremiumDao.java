package com.emids.healthinsurence.dao;

import java.util.List;

import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.model.Habits;

public interface InsurancePremiumDao {
    public  int getAmount(int amount);
    public void setAmount(int amount);    
    public Customer getCustomerById(int cus_id);  
    public Habits getHabitsById(int cus_id);
    public CurrentHealth getCurrentHealthById(int cus_id);
    public List<Customer> listCustomer();
}
