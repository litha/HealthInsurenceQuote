package com.emids.healthinsurence.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.emids.healthinsurance.dao.impl.InsurancePremiumDaoImpl;
import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.Customer;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurance.service.InsurencePremiumService;
import com.emids.healthinsurence.dao.InsurancePremiumDao;

@Service("insurencePremiumService")
public class InsurencePremiumServiceImpl implements InsurencePremiumService {

	private static final Logger logger = LoggerFactory.getLogger(InsurencePremiumServiceImpl.class);
	@Autowired
	private InsurancePremiumDao insurancePremiumDao;

	public void setInsurancePremiumDao(InsurancePremiumDao insurancePremiumDao) {
		this.insurancePremiumDao = insurancePremiumDao;
	}

	@Transactional
	public Customer getCustomerById(int cus_id) {
		logger.info("fomr CUS ID");
		return this.insurancePremiumDao.getCustomerById(cus_id);
	}

	@Transactional
	public Habits getHabitsById(int cus_id) {
		return this.insurancePremiumDao.getHabitsById(cus_id);
	}

	@Transactional
	public CurrentHealth getCurrentHealthById(int cus_id) {
		return this.insurancePremiumDao.getCurrentHealthById(cus_id);
	}

	// To Calculate Premium
	@Transactional
	public double getinsurencePremium() {
		// Base Premium
		InsurancePremiumDaoImpl pm = new InsurancePremiumDaoImpl();
		pm.setAmount(5000);
		double basicAmount = pm.getAmount(5000);
		double calculation = 0;

		/*
		 * % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 ->
		 * +10% | 35-40 -> +10% | 40+ -> 20%
		 */

		if (getCustomerById(1).getAge() <= 18) {
			System.out.println("Below 18 Years" + " " + getCustomerById(1).getName() + " " + pm.getAmount(5000));
		} else if (getCustomerById(1).getAge() > 18 && getCustomerById(1).getAge() <= 25) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else if (getCustomerById(1).getAge() > 25 && getCustomerById(1).getAge() <= 30) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else if ((getCustomerById(1).getAge() > 30) && (getCustomerById(1).getAge() <= 35)) {

			calculation = basicAmount + (basicAmount * 10) / 100;

		} else if (getCustomerById(1).getAge() > 35 && getCustomerById(1).getAge() <= 40) {
			calculation = basicAmount + (basicAmount * 10) / 100;
		} else {
			calculation = basicAmount + (basicAmount * 20) / 100;
		}

		// Gender rule: Male vs female vs Other
		// Males

		if (getCustomerById(1).getGender().equalsIgnoreCase("M")) {
			calculation = calculation + (calculation * 2) / 100;
		} else {
			calculation = basicAmount;
		}

		// calculation based on Pre-existing conditions

		if (getCurrentHealthById(1).getHypertension() == 1) {
			calculation = calculation + (calculation * 1) / 100;
		} else if (getCurrentHealthById(1).getBloodPressure() == 1) {
			calculation = calculation + (calculation * 1) / 100;
		} else if (getCurrentHealthById(1).getOverweight() == 1) {
			calculation = calculation + (calculation * 1) / 100;
		} else if (getCurrentHealthById(1).getBloodSugar() == 1) {
			calculation = calculation + (calculation * 1) / 100;
		}

		/*
		 * Good habits
		 */
		if (getHabitsById(1).getDailyExercise() == 1) {
			calculation = calculation - (calculation * 3) / 100;
		} else if (getHabitsById(1).getAlcohol() == 1) {
			calculation = calculation + (calculation * 3) / 100;
		} else if (getHabitsById(1).getSmoking() == 1) {
			calculation = calculation + (calculation * 3) / 100;
		} else if (getHabitsById(1).getDrugs() == 1) {
			calculation = calculation + (calculation * 3) / 100;
		}
		return calculation;
	}

	@Transactional
	public List<Customer> listCustomer() {
		return this.insurancePremiumDao.listCustomer();
	}
}